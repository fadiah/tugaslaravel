@extends('layouts.master')

@section('content')
			<body style="background-color: pink;">
			<h2 style="font-family: Courier New Lucida Console;text-shadow: 3px 3px grey;">Edit Data Siswa</h1>
			@if(session('sukses'))
			<div class="alert alert-light" role="alert">
			 {{session('sukses')}}
			</div>
			@endif
			
			<div class="row">
				<div class="col-lg-12" >
				<form action="/siswa/{{$siswa->id}}/update" method="POST">
								{{csrf_field()}}

								<div class="form-group">
								    <label for="exampleInputEmail1"><b>Nama Depan</b></label>
								    <input name="nama_depan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Depan" value="{{$siswa->nama_depan}}" style="background-color: black;color: pink;width: 35%">
								</div>

								<div class="form-group">
								    <label for="exampleInputEmail1"><b>Nama Belakang</b></label>
								    <input name="nama_belakang" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Belakang" value="{{$siswa->nama_belakang}}" style="background-color: black;color: pink;width: 35%">
								</div>
								<img src="/css/gif.png" width=”250px″ height="250px" align="right" >
								<div class="form-group">
									 <label for="exampleFormControlSelect1"><b>Pilih Jenis Kelamin</b></label>
									 <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1" style="background-color: black;color: pink;width: 35%">
									   <option value="L" @if($siswa->jenis_kelamin == 'L') selected @endif>Laki-laki</option>
									   <option value="P" @if($siswa->jenis_kelamin == 'P') selected @endif>Perempuan</option>
									 </select>
								</div>

								<div class="form-group">
									 <label for="exampleInputEmail1"><b>Agama</b></label>
									 <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama" value="{{$siswa->agama}}" style="background-color: black;color: pink;width: 35%">
								</div>

								 <div class="form-group">
									 <label for="exampleFormControlTextarea1"><b>Alamat</b></label>
									 <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" style="background-color: black;color: pink;width: 35%">{{$siswa->alamat}}</textarea>
								</div>
								<button type="submit" class="btn" style="background-color: #ffff66;">Update</button>
						</form>


					</div>
			</div>


	</body>

	@endsection