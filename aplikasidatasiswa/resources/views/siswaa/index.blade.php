@extends('layouts.master')

@section('content')

			@if(session('sukses'))
			<div class="alert alert-light" role="alert">
			 {{session('sukses')}}
			</div>
			@endif
			<div class="row">
				<div class="col-6">
					<h1 style="font-family: Courier New Lucida Console;">Data Siswa</h1>
				</div>
				<div class="col-6">
									
					<button type="button" class="bg-white btn btn-xs float-right" data-toggle="modal" data-target="#exampleModal">
					  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 64 64" enable-background="new 0 0 64 64" xml:space="preserve">
						<g id="CIRCLE__x2F__PLUS_1_" enable-background="new    ">
							<g id="CIRCLE__x2F__PLUS">
								<g>
									<path d="M45,29H35V19c0-1.657-1.343-3-3-3s-3,1.343-3,3v10H19c-1.657,0-3,1.343-3,3s1.343,3,3,3h10v10c0,1.657,1.343,3,3,3     s3-1.343,3-3V35h10c1.657,0,3-1.343,3-3S46.657,29,45,29z M32,0C14.327,0,0,14.327,0,32s14.327,32,32,32s32-14.327,32-32     S49.673,0,32,0z M32,58C17.641,58,6,46.359,6,32C6,17.64,17.641,6,32,6c14.359,0,26,11.641,26,26C58,46.359,46.359,58,32,58z"/>
								</g>
							</g>
						</g>
					</svg>
					</button>

				

			
				</div>
				
				<table class="table" style="background-color: pink;
 				 border-radius: 25px;">

					<thead style="background-color: black;color: white;">
					<tr>
						<th>Nama Depan</th>
						<th>Nama Belakang</th>
						<th>Jenis Kelamin</th>
						<th>Agama</th>
						<th>Alamat</th>
						<th>AKSI</th>
					</tr>
				</thead>
					@foreach($data_siswa as $siswa)
					<tr>
						<td>{{$siswa->nama_depan}}</td>
						<td>{{$siswa->nama_belakang}}</td>
						<td>{{$siswa->jenis_kelamin}}</td>
						<td>{{$siswa->agama}}</td>
						<td>{{$siswa->alamat}}</td>
						<td><a href="/siswa/{{$siswa->id}}/edit" class="btn btn-sm" style="background-color: #ffff66;">Edit</a>
							<a href="/siswa/{{$siswa->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin ni dihapus?')">Delete</a>
						</td>
					</tr>
					@endforeach
				</table>
			</div>
		</div>
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header" style="background-color: pink;">
						<h5 class="modal-title" id="exampleModalLabel" style="color: black;font-family: Courier New Lucida Console;"><b>Tambah Data Siswa</b></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

						<div class="modal-body" style="background-color: black;">
							<form action="/siswa/create" method="POST">
								{{csrf_field()}}
								<div class="form-group">
								    <label for="exampleInputEmail1" style="color: pink">Nama Depan</label>
								    <input name="nama_depan" type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Depan" style="background-color: black;color: pink;">
								</div>

								<div class="form-group">
								    <label for="exampleInputEmail1" style="color: pink">Nama Belakang</label>
								    <input name="nama_belakang" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Belakang"style="background-color: black;color: pink;">
								</div>

								<div class="form-group">
									 <label for="exampleFormControlSelect1" style="color: pink">Pilih Jenis Kelamin</label>
									 <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1" style="background-color: black;color: pink;">
									   <option value="L" >Laki-laki</option>
									   <option value="P" >Perempuan</option>
									 </select>
								</div>

								<div class="form-group">
									 <label for="exampleInputEmail1" style="color: pink">Agama</label>
									 <input name="agama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama" style="background-color: black;color: pink;">
								</div>

								 <div class="form-group">
									 <label for="exampleFormControlTextarea1" style="color: pink">Alamat</label>
									 <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3" style="background-color: black;color: pink;"></textarea>
								</div>

								  
							
						</div>

						<div class="modal-footer" style="background-color: black">
							<button type="button" class="btn" style="background-color: black;color: pink;" data-dismiss="modal"><b>Close</b></button>
							<button type="submit" class="btn" style="background-color: pink;"><b>Submit</b></button>
							</form>
						</div>
					</div>
				</div>
@endsection		


